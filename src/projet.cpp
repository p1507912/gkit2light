
//! \file shader_kit.cpp shader_kit light, bac a sable fragment shader, cf shader_toy

#include <cstdio>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <time.h>

#include "glcore.h"
#include "window.h"
#include "app.h"

#include "program.h"
#include "uniforms.h"

#include "texture.h"
#include "mesh.h"
#include "wavefront.h"

#include "vec.h"
#include "mat.h"
#include "orbiter.h"
#include "draw.h"

#include "text.h"
#include "widgets.h"

// Code C++ -> GLSL
// int : GLint -> int
// float : GLfloat -> float
// Vecteur 2/3/4 : vec2/3/4 -> vec2/3/4
// Matrice : Transform -> mat4

struct Triangle
{
    Point a;
    vec3 ab;
    vec3 ac;
    Color diffuse;
    float shininess;
};

struct Sphere
{
    Point c;
    double r;
    Color diffuse;
    float shininess;
};

struct PointLight
{
    Point position;
    float intensity;
    float radius;
    Color color;
};

struct GameObject {
    Mesh mesh;
    Vector position;
    Vector rotation;
    Color diffuse;
    float shininess;
    std::vector<Triangle*> triangles;

    GameObject(const char* path) {
        mesh = read_mesh(path);
        position = Vector(0, -1, 0);
        rotation = Vector(0, -1, 0);
        diffuse = Color(1, 1, 0);
        shininess = 0;
    }

    GameObject(const char* path, Vector _position = Vector(0, 0, 0), Vector _rotation = Vector(0, 0, 0)) {
        mesh = read_mesh(path);
        position = _position;
        rotation = _rotation;
        diffuse = Color(1, 1, 0);
        shininess = 0;
    }

    void init_app(std::vector<Triangle>* app_triangles) {
        Transform rot = (RotationX(rotation.x) * RotationY(rotation.y) * RotationZ(rotation.z));
        Transform trans = Translation(position);
        for (int i = 0; i < mesh.triangle_count(); i++) {
            Triangle t_mesh;
            t_mesh.diffuse = diffuse;
            t_mesh.shininess = shininess;
            t_mesh.a = (trans * rot)(Point(mesh.triangle(i).a));
            t_mesh.ab = rot(Vector(mesh.triangle(i).b) - Vector(mesh.triangle(i).a));
            t_mesh.ac = rot(Vector(mesh.triangle(i).c) - Vector(mesh.triangle(i).a));
            app_triangles->push_back(t_mesh);

            triangles.emplace_back(&(*app_triangles)[app_triangles->size() - 1]); // Pointeur sur le triangle dans le grand tableau des triangles
        }
    }

    void update() {
        Transform rot = (RotationX(rotation.x) * RotationY(rotation.y) * RotationZ(rotation.z));
        Transform trans = Translation(position);
        for (int i = 0; i < triangles.size(); i++) {
            triangles[i]->a = (trans * rot)(Point(mesh.triangle(i).a));
            triangles[i]->ab = rot(Vector(mesh.triangle(i).b) - Vector(mesh.triangle(i).a));
            triangles[i]->ac = rot(Vector(mesh.triangle(i).c) - Vector(mesh.triangle(i).a));
        }
    }
};

class MyApp : public App
{

private:
    std::vector<Sphere> spheres; // Toutes les spheres
    std::vector<PointLight> point_lights; // Toutes les point light
    std::vector<Triangle> triangles; // Tout les triangles
    Orbiter m_camera; // La camera de la scene

    //float m_x, m_y, m_z;
    DrawParam gl;
    Mesh _renderer;
    GLuint m_program;

    float elapsedTime;

    float hh, ww, pp;

    Point win_position;

    std::vector<GameObject> gameObjects;
    std::string scene;

    int nb_scene;

public:
    MyApp(std::string sc) : App(1280, 720) {
        triangles.reserve(300);
        scene = sc;
    }

    // Envoie des information des point light au shader
    void sendPointLightsToShader()
    {
        vec3 _point_light_pos[10];          // Les positions des lumieres
        vec3 _point_light_color[10];        // Les couleurs des lumieres
        GLfloat _point_light_intensity[10]; // L'intensite des lumieres
        GLfloat _point_light_radius[10];    // Le rayon des lumieres

        for (int i = 0; i < point_lights.size(); i++)
        {
            _point_light_pos[i] = vec3(point_lights[i].position.x, point_lights[i].position.y, point_lights[i].position.z);
            _point_light_color[i] = vec3(point_lights[i].color.r, point_lights[i].color.g, point_lights[i].color.b);
            _point_light_intensity[i] = point_lights[i].intensity;
            _point_light_radius[i] = point_lights[i].radius;
        }

        GLint _point_light_pos_location = glGetUniformLocation(m_program, "_point_light_pos");
        GLint _point_light_color_location = glGetUniformLocation(m_program, "_point_light_color");
        GLint _point_light_intensity_location = glGetUniformLocation(m_program, "_point_light_intensity");
        GLint _point_light_radius_location = glGetUniformLocation(m_program, "_point_light_radius");

        glUniform3fv(_point_light_pos_location, point_lights.size(), &_point_light_pos[0].x);
        glUniform3fv(_point_light_color_location, point_lights.size(), &_point_light_color[0].x);
        glUniform1fv(_point_light_intensity_location, point_lights.size(), &_point_light_intensity[0]);
        glUniform1fv(_point_light_radius_location, point_lights.size(), &_point_light_radius[0]);

        program_uniform(m_program, "_point_light_SIZE", (GLint)point_lights.size());
    }

    // Envoie des information des spheres au shader
    void sendSpheresToShader()
    {
        vec3 _sphere_c[10];            // Les positions des spheres
        GLfloat _sphere_r[10];         // Les rayons des spheres, just "float" ca marche aussi
        vec3 _sphere_diffuse[10];      // Les couleurs des spheres
        GLfloat _sphere_shininess[10]; // La brillance des spheres

        for (int i = 0; i < spheres.size(); i++)
        {
            _sphere_c[i] = vec3(spheres[i].c.x, spheres[i].c.y, spheres[i].c.z);
            _sphere_r[i] = spheres[i].r;
            _sphere_diffuse[i] = vec3(spheres[i].diffuse.r, spheres[i].diffuse.g, spheres[i].diffuse.b);
            _sphere_shininess[i] = spheres[i].shininess;
        }

        GLint _sphere_c_location = glGetUniformLocation(m_program, "_sphere_c");
        GLint _sphere_r_location = glGetUniformLocation(m_program, "_sphere_r");
        GLint _sphere_shininess_location = glGetUniformLocation(m_program, "_sphere_shininess");
        GLint _sphere_diffuse_location = glGetUniformLocation(m_program, "_sphere_diffuse");
        glUniform3fv(_sphere_c_location, spheres.size(), &_sphere_c[0].x);
        glUniform1fv(_sphere_r_location, spheres.size(), &_sphere_r[0]);
        glUniform1fv(_sphere_shininess_location, spheres.size(), &_sphere_shininess[0]);
        glUniform3fv(_sphere_diffuse_location, spheres.size(), &_sphere_diffuse[0].x);

        program_uniform(m_program, "_sphere_SIZE", (GLint)spheres.size()); // On envoie aussi le nombre de spheres
    }

    // Envoie des information des triangles au shader
    void sendTriangleToShader()
    {
        vec3 _triangle_a[300]; // Position du point A du triangle ABC
        vec3 _triangle_ab[300]; // Vecteur AB
        vec3 _triangle_ac[300]; // Vecteur AC
        vec3 _triangle_diffuse[300]; // Couleur du triangle
        GLfloat _triangle_shininess[300]; // Brillance du triangle

        for (int i = 0; i < std::min(triangles.size(), (size_t)300); i++)
        {
            _triangle_a[i] = vec3(triangles[i].a.x, triangles[i].a.y, triangles[i].a.z);
            _triangle_ab[i] = triangles[i].ab;
            _triangle_ac[i] = triangles[i].ac;
            _triangle_diffuse[i] = vec3(triangles[i].diffuse.r, triangles[i].diffuse.g, triangles[i].diffuse.b);
            _triangle_shininess[i] = triangles[i].shininess;
        }

        GLint _triangle_a_location = glGetUniformLocation(m_program, "_triangle_a");
        GLint _triangle_ab_location = glGetUniformLocation(m_program, "_triangle_ab");
        GLint _triangle_ac_location = glGetUniformLocation(m_program, "_triangle_ac");
        GLint _triangle_diffuse_location = glGetUniformLocation(m_program, "_triangle_diffuse");
        GLint _triangle_shininess_location = glGetUniformLocation(m_program, "_triangle_shininess");

        glUniform3fv(_triangle_a_location, std::min(triangles.size(), (size_t)300), &_triangle_a[0].x);
        glUniform3fv(_triangle_ab_location, std::min(triangles.size(), (size_t)300), &_triangle_ab[0].x);
        glUniform3fv(_triangle_ac_location, std::min(triangles.size(), (size_t)300), &_triangle_ac[0].x);
        glUniform3fv(_triangle_diffuse_location, std::min(triangles.size(), (size_t)300), &_triangle_diffuse[0].x);
        glUniform1fv(_triangle_shininess_location, std::min(triangles.size(), (size_t)300), &_triangle_shininess[0]);

        program_uniform(m_program, "_triangle_SIZE", (GLint)std::min(triangles.size(), (size_t)300));
    }

    // Envoi des information de la scene au shader
    void sendToShader()
    {
        sendSpheresToShader();
        sendPointLightsToShader();
        sendTriangleToShader();
    }

    // Creation des mur et du sol de la scene
    void defineSpheres() {
        Sphere sol;
        sol.r = 500;
        sol.c = Point(0, -sol.r, 0);
        sol.diffuse = Color(1, 1, 1);
        sol.shininess = 0.1;

        Sphere mur_gauche;
        mur_gauche.r = 500;
        mur_gauche.c = Point(RotationY(-45)(Vector(mur_gauche.r, 0, 0)));
        mur_gauche.diffuse = Color(1, 1, 1);
        mur_gauche.shininess = 0.1;

        Sphere mur_droit;
        mur_droit.r = 500;
        mur_droit.c = Point(RotationY(-135)(Vector(mur_droit.r, 0, 0)));
        mur_droit.diffuse = Color(1, 1, 1);
        mur_droit.shininess = 0.1;

        spheres.push_back(sol);
        spheres.push_back(mur_gauche);
        spheres.push_back(mur_droit);
    }

    void init_coureur() {
        Vector objRot = Vector(0, 45, 0);

        gameObjects.emplace_back(GameObject("./data/leg_corps.obj", Vector(4, 1, -10), objRot));
        gameObjects.emplace_back(GameObject("./data/leg.obj", Vector(-5, 1, -10.75f), objRot));
        gameObjects.emplace_back(GameObject("./data/arm.obj", vec3(-11, 3, -20), objRot));

        Sphere tete;
        tete.r = -0.5f;
        tete.c = Point(8, 4.5f, -21.5);
        tete.diffuse = Color(0, 0, 1);
        tete.shininess = 0.1;
        spheres.push_back(tete);

        // gagnant : x:15.54, y:1.94, z:-22.81
        win_position = Point(15.54f, 1.94f, -22.81f);
    }

    void init_anonymous() {
        Vector objRot = Vector(0, 45, 0);

        gameObjects.emplace_back(GameObject("./data/moustache.obj", Vector(2, 12, -20), objRot));
        gameObjects.emplace_back(GameObject("./data/bouc.obj", Vector(2, 6, -20), objRot));
        gameObjects.emplace_back(GameObject("./data/bouche.obj", Vector(0, 10, -22), -objRot));

        Sphere right_eye;
        right_eye.r = 1;
        right_eye.c = Point(-1, 16, -20);
        right_eye.diffuse = Color(0, 0, 1);
        right_eye.shininess = 0.1;

        Sphere left_eye;
        left_eye.r = 1.1f;
        left_eye.c = Point(5, 18, -22);
        left_eye.diffuse = Color(0, 0, 1);
        left_eye.shininess = 0.1;

        spheres.push_back(left_eye);
        spheres.push_back(right_eye);

        //gagnant : x:-11.89, y:5.3, z:-33.27
        win_position = Point(-11.89f, 5.3, -33.27f);
    }
	
	void init_batman() {

        Vector objRot = Vector(0, -45, 0);
        Vector objRot2 = Vector(0, 135, 0);
        Vector objRot3 = Vector(0, -32, 0);
        Vector objRot4 = Vector(0, -78, 0);

        gameObjects.emplace_back(GameObject("./data/batman_base.obj", Vector(-6, 16.5f, -25), objRot));
        gameObjects.emplace_back(GameObject("./data/batman_aile_gauche.obj", Vector(-4, 17.6, -30.6), objRot));
        gameObjects.emplace_back(GameObject("./data/batman_bas.obj", Vector(5, 16.4, -32.1), objRot2));
        gameObjects.emplace_back(GameObject("./data/batman_aile_droite.obj", Vector(5.5, 16.8, -31.1), objRot2));
        gameObjects.emplace_back(GameObject("./data/batman_oreil.obj", Vector(2.5, 16.8, -30.4), objRot3));
        gameObjects.emplace_back(GameObject("./data/batman_oreil.obj", Vector(2.5, 16.7, -30.9), objRot4));

        Sphere tete;
        tete.r = 0.4f;
        tete.c = vec3(2, 17.5, -30.3);
        tete.diffuse = Color(0, 0, 1);
        tete.shininess = 0.1;

        spheres.push_back(tete);
        
        // x:8.51, y : 17.02, z : -35.13
        win_position = Point(8.51f , 17.02f, -35.13f);
    }

    int init() {
        nb_scene = 3;

        ww = -2;
        hh = 7;
        pp = -20;

        if(scene == "coureur")
            init_coureur();
        else if(scene == "anonymous")
            init_anonymous();
		else if(scene == "batman")
			init_batman();
        else {
            int r = rand() % nb_scene;
            switch(r) {
                case 0:
                    init_coureur();
                    break;
                case 1:
                    init_anonymous();
                    break;
				case 2:
                    init_batman();
                    break;
            }
        }
            
        defineSpheres();
        definePointLights();

        for (int i = 0; i < gameObjects.size(); i++) {
            gameObjects[i].diffuse = Color(0, 0, 1);
            gameObjects[i].init_app(&triangles);
        }

        m_program = read_program("data/shaders/my_trace.glsl"); // Chargement du shader
        program_print_errors(m_program);

        elapsedTime = 0.0f;

        _renderer = Mesh(GL_TRIANGLE_STRIP);
        _renderer.vertex(Point(0, 0, 0));
        _renderer.vertex(Point(0, 0.1, 0));
        _renderer.vertex(Point(0.1, 0, 0));

        // Positionnement de la camera
        m_camera.lookat(Point(0, 0, 0), 75);
        m_camera.rotation(0, 35);

        gl.camera(m_camera);

        return 0;
    }

    // Creation de la points light de la scene
    void definePointLights()
    {
        PointLight pl;
        pl.position = Point(ww, hh, pp);

        pl.radius = 50;
        pl.intensity = 2;
        pl.color = Color(1, 1, 1);

        point_lights.push_back(pl);
    }

    // destruction des objets de l'application
    int quit()
    {
        release_program(m_program);
        return 0;
    }

    // Gestion des evenements clavier
    void events()
    {
        int mx, my;
        unsigned int mb = SDL_GetRelativeMouseState(&mx, &my);
        if (mb & SDL_BUTTON(1)) // le bouton gauche est enfonce
            m_camera.rotation(mx, my);
        else if (mb & SDL_BUTTON(3)) // le bouton droit est enfonce
            m_camera.move(mx);
        else if (mb & SDL_BUTTON(2)) // le bouton du milieu est enfonce
            m_camera.translation((float)mx / (float)window_width(), (float)my / (float)window_height());

        if (key_state(SDLK_h))
        {
            for (unsigned int i = 0; i < spheres.size() - 2; i++)
            { // -2 car les 2 dernieres sont utilisés pour le sol et le mur
                std::cout << "Sphere " << i + 1 << ": x:" << spheres.at(i).c.x
                          << ", y: " << spheres.at(i).c.y
                          << ", z: " << spheres.at(i).c.z << std::endl;
            }
            for (unsigned int i = 0; i < triangles.size(); i++)
            {
                std::cout << "Triangle " << i + 1 << ": x:" << triangles.at(i).a.x
                          << ", y: " << triangles.at(i).a.y
                          << ", z: " << triangles.at(i).a.z << std::endl;
            }
            std::cout << "Pointlight: x:" << ww << ", y:" << hh << ", z:" << pp << std::endl;
            clear_key_state(SDLK_h);
        }

        if (key_state(SDLK_e))
            hh += 0.01f * delta_time();
        if (key_state(SDLK_a))
            hh -= 0.01f * delta_time();
        if (key_state(SDLK_q)) {
            pp -= 0.01f * delta_time();
            ww += 0.01f * delta_time();
        }
        if (key_state(SDLK_d)) {
            pp += 0.01f * delta_time();
            ww -= 0.01f * delta_time();
        }
        if (key_state(SDLK_z)) {
            pp += 0.01f * delta_time();
            ww += 0.01f * delta_time();
        }
        if (key_state(SDLK_s)) {
            pp -= 0.01f * delta_time();
            ww -= 0.01f * delta_time();
        }
    }

    // dessiner une nouvelle image
    int render()
    {
        events();

        //for (int i = 0; i < gameObjects.size(); i++)
            //gameObjects[i].update();

        point_lights[0].position = Point(ww, hh, pp);
        point_lights[0].color = length(win_position - point_lights[0].position) < 1 ? Color(0, 1, 0) : Color(1, 1, 1);

        glUseProgram(m_program);

        Transform model = Identity();
        Transform view = m_camera.view();
        Transform proj = m_camera.projection(window_width(), window_height(), 45);
        Transform mvp = proj * view * model;
        Transform mvpInv = Inverse(mvp);

        program_uniform(m_program, "mvpInvMatrix", mvpInv);
        sendToShader();
        _renderer.draw(m_program, true, true, true, true);

        glClearColor(0, 0, 0, 1);

        elapsedTime += delta_time();

        return 1;
    }
};

int main(int argc, char **argv)
{
    srand(time(NULL));

    std::string scene = argv[argc-1];
    std::cout << scene << std::endl;
    MyApp myapp(scene);
    myapp.run();

    return 0;
}
