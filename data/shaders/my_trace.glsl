#version 330

#ifdef VERTEX_SHADER
out vec2 position;

void main( )
{
    vec2 positions[3]= vec2[3]( vec2(-1,-3), vec2(3, 1), vec2(-1, 1) );
    
    position= positions[gl_VertexID];
    gl_Position= vec4(positions[gl_VertexID], 0, 1);
}
#endif

#ifdef FRAGMENT_SHADER

uniform int _sphere_SIZE;
uniform vec3 _sphere_c[10];
uniform float _sphere_r[10];
uniform float _sphere_shininess[10];
uniform vec3 _sphere_diffuse[10];

uniform int _point_light_SIZE;
uniform vec3 _point_light_pos[10];
uniform vec3 _point_light_color[10];
uniform float _point_light_intensity[10];
uniform float _point_light_radius[10];

uniform int _triangle_SIZE;
uniform vec3 _triangle_a[300];
uniform vec3 _triangle_ab[300];
uniform vec3 _triangle_ac[300];
uniform vec3 _triangle_diffuse[300];
uniform float _triangle_shininess[300];

uniform float time;
uniform vec3 motion;    // x, y, button > 0
uniform vec3 mouse;     // x, y, button > 0
uniform vec2 viewport;

/*
catalogue de fonctions sur les spheres :
http://www.iquilezles.org/www/articles/spherefunctions/spherefunctions.htm
*/

#define inf 999999.0

#define TYPE_NONE -1
#define TYPE_SPHERE 0
#define TYPE_TRIANGLE 1

// Calcul de collision avec un triangle
// o : Origine du rayon
// d : direction du rayon
// a : Point  A du triangle ABC
// ab : Segment AB
// ac : Segment AC
float triangle ( const in vec3 o, const in vec3 d, const in vec3 a, const in vec3 ab, const in vec3 ac) {    
    float res = inf;

    vec3 pvec = cross(d, ac);
    float det = dot(ab, pvec);
    float inv_det = 1/det;
    
    vec3 uvec = o - a;
    float u = dot(uvec, pvec) * inv_det;
    if(u < 0 || u > 1)
        return res;

    vec3 vvec = cross(uvec, ab);
    float v = dot(d, vvec) * inv_det;
    if(v < 0 || u +  v > 1)
        return res;

    float t = dot(ac, vvec) * inv_det;

    if(t > 0) {
        res = t;
    }

    return res;
}

// Calcul de collision avec une sphere
// o : Origine du rayon
// d : direction du rayon
// center : centre de la sphere
// radius : rayon de la sphere
float sphere( const in vec3 o, const in vec3 d, const in vec3 center, const in float radius)
{
    vec3 oc= o - center;
    float b= dot(oc, d);
    float c= dot(oc, oc) - radius*radius;
    float h= b*b - dot(d, d) * c;
    if(h < 0.0)
        return inf;
    
    h= sqrt(h);
    float t1= (-b - h);

    float t2= (-b + h);

    if(t1 <= 0 && t2 <= 0)
        return inf;
    else if(t1 > 0 && t2 > 0)
        return min(t1, t2) / dot(d, d); // plus petite racine postive
    else
        return max(t1, t2) / dot(d, d); // racine positive

    if(t1 < 0)
        return inf;
    else
        return t1 / dot(d, d);
}

// Detection de collision avec un object de la scene
// o : Origine du rayon
// d : Direction du rayon
// t : 
// n : Normale de la surface touchee
// c : Couleur de la surface touchee
// shininess : Brillance de la surface touchee
// index : Identifiant de l'objet touch��
// type : Type de l'objet touch�e (sphere ou triangle)
bool object( const in vec3 o, const in vec3 d, out float t, out vec3 n, out vec3 c, out float shininess, out int index, out int type)
{
    type = TYPE_NONE;

    n= vec3(0, 1, 1);
    t = inf;

    bool res = false;

    int return_id = 0;

    for(int i = 0; i < _triangle_SIZE; i++) {
        vec3 a = _triangle_a[i];
        vec3 ab = _triangle_ab[i];
        vec3 ac = _triangle_ac[i];
        float t1 = triangle(o, d, a, ab, ac);

        if(t1 < t) {
            t = t1;
            index = return_id;
            res = true;
            n= normalize(cross(ab, ac));
            c = _triangle_diffuse[i];
            shininess = _triangle_shininess[i];
            type = TYPE_TRIANGLE;
        }
        return_id++;
    }

    for(int i = 0; i < _sphere_SIZE; i++) {
        vec3 center = _sphere_c[i];
        float radius= _sphere_r[i];       
        float t1;
        t1= sphere(o, d, center, radius);
        
        if(t1 < t) {
            t = t1;
            index = return_id;
            res = true;
            n= normalize(o + t1*d - center);
            shininess = _sphere_shininess[i];
            type = TYPE_SPHERE;

            c = _sphere_diffuse[i];
        }
        return_id++;
    }

    return res;
}

uniform mat4 mvpInvMatrix;
uniform mat4 mvMatrix;

in vec2 position;

out vec4 fragment_color;

void main( )
{
    // construction du rayon pour le pixel, passage depuis le repere projectif
    vec4 oh= mvpInvMatrix * vec4(position, 0, 1);       // origine sur near
    vec4 eh= mvpInvMatrix * vec4(position, 1, 1);       // extremite sur far
    
    // origine et direction
    vec3 o= oh.xyz / oh.w;                              // origine
    vec3 d= eh.xyz / eh.w - oh.xyz / oh.w;              // direction
    d= normalize(d);
    
    vec3 n;
    vec3 c;
    float shininess;
    float t;
    int type;
    int index = -1;

    // Si on touche un objet
    if(object(o, d, t, n, c, shininess, index, type))
    {
        vec3 p= o + t*d;
        vec3 finalcolor = vec3(0);

        // On regarde si on est expos� aux couleurs de la scene
        for(int j = 0; j < _point_light_SIZE; j++) {
            
            vec3 light = normalize(_point_light_pos[j] - p);
            vec3 shadown;
            vec3 shadowc;
            float shadow;
            float shadows;
            int shadowtype;
            int index2 = -1;

            bool hit= object(p + n * 0.0001, _point_light_pos[j] - p, shadow, shadown, shadowc, shadows, index2, shadowtype);

            float dist = length(_point_light_pos[j] - p);

            float attenuation = max(0.0f, (_point_light_radius[j] - dist) / (_point_light_radius[j] / _point_light_intensity[j]));

            vec3 reflectDir = reflect(light, n);
            float specular = pow(max(dot(d, reflectDir), 0.0), 1/shininess) * attenuation;
            vec3 specularColor = specular * vec3(1,1,1);
        
            vec3 color = c * _point_light_color[j]* max(0, dot(n, light)) * attenuation;

            if(hit && index != index2) {
                //if (shadowtype == TYPE_TRIANGLE && dot(normalize(_point_light_pos[j] - p), shadown) < 0) {

                //} else {
                    vec3 pp = (p + n * 0.0001) + shadow * (_point_light_pos[j] - p);

                    if (length(p - pp) < dist) { // A l'ombre
                        color = color*0.3;
                        specularColor = vec3(0);
                    } 
                //}
            }

            finalcolor += color + specularColor;
        }

        fragment_color= vec4(finalcolor, 1);
    } else {
        fragment_color= vec4(0, 0, 0, 1);
    }
}
#endif
