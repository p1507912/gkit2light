# Ombres chinoises

Virgile ARAGON
Yannick KWANSA

## Presentation

Pour ce projet de synthèse d'image, nous avons réalisé un jeu ou le joueur doit déplacer une lumière pour faire apparaitre un motif sur un mur.  
Une fois la lumière bien placer dans l'espace, elle change de couleur et éclaire la pièce en vert.  

Le jeu propose trois scenes différentes :
- coureur : une silouhette d'athlète
- anonymous : un visage moustachu
- batman : une silouhette du super héro

### Commandes

- Z pour avancer vers le mur de gauche
- S pour s'éloigner du mur de gauche
- D pour avancer vers le mur de droite
- Q Pour s'eloigner du mur de droite
- A pour baisser la hauteur de la lumière
- E pour augmenter la hauteur de la lumière

### Organisation hierarchique du projet

- src/projet.cpp : Code source du projet.
- data/my_trace.glsl : Shader pour le lancer de rayon.
- data/*.obj : Ensemble des maillages de chaques scènes.

### Démarrer le projet

Vous pouvez créer un projet pour différentes plateforme.  
Sur Linux ou Windows, lancez premake avec la plateforme de votre choix pour créer le projet.  
  
Pour le lancer sur linux avec une scène au choix :
```
./bin/projet <nom_de_la_scene>
```
Pour le lancer sur linux avec une scène au hasard :
```
./bin/projet
```

Sur Windows, vous pouvez créer le projet avec l'option "vs2017" et premake et lancer "gkit2.sln", et selectionner la solution "projet".  
Pour passer une scène en argument, clique droit sur la solution, "Paramatre" -> "Debogage" -> "Arguments de la commande" et ecrire le nom de la scène.
